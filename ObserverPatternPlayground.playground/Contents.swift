import Foundation

// MARK: - Metods of the object of observer

// Протокол, который реализует объект наблюдения

protocol Subject {
    func add(observer: PropertyObserver)  // добавить наблюдателя
    func remove(observer: PropertyObserver)  // удалить наблюдателя
    func notify(withString string: String)  // уведомить наблюдателя о чем-то
}

// MARK: - Observation object

// Объект за которым будут наблюдать другие объекты, подписан на протокол кот реализует объект наблюдения

class Teacher: Subject {
    
    // коллекция в которую добавляются наблюдатели:
    var observerCollection = NSMutableSet()
    // Новое значение помещаем в notify, который у всех наблюдателей вызовет didGet
    var homeTask = "" {
        didSet{
            notify(withString: homeTask)
        }
    }
    
    func add(observer: PropertyObserver) {
        observerCollection.add(observer)
    }
    
    func remove(observer: PropertyObserver) {
        observerCollection.remove(observer)
    }
    
    func notify(withString string: String) {
        for observer in observerCollection {
            (observer as! PropertyObserver).didGet(newTask: string)
        }
    }
    
}


// MARK: - Actions on notification

// Протокол, который реализуют все наблюдатели
// То, что должен сделать наблюдатель, когда он получит уведомление

protocol PropertyObserver {
    func didGet(newTask: String)
}


// MARK: - Observers

// Наследуемся от NSObject только для помещения в коллекцию NSMutableSet()

class Pupil: NSObject, PropertyObserver {
    
    var homeTask = ""
    
    func didGet(newTask task: String) {
        homeTask = task
        print("new homework")
    }
}


// MARK: - Check work

let teacher = Teacher()
let pupil = Pupil()

teacher.add(observer: pupil)
teacher.homeTask = "homework #2"

pupil.homeTask
print(pupil.homeTask)
